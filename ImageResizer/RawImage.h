#pragma once

#include "Color.h"
#include "Array.h"

namespace ir {

	using namespace std;

	class RawImage : public Array<Color> {

	public:
		RawImage(const size_t width, const size_t height = 1) :
			Array(width, height) {}

		void scale(const size_t newWidth, const size_t newHeight);

	};

}
