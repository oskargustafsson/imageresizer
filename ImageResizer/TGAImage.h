#pragma once

#include "RawImage.h"
#include "Array.h"

namespace ir {

	using namespace std;

	class TGAImage {

	public:
		TGAImage(ifstream& ifs);
		void scale(const float scale);
		void scale(const uint16_t newWidth, const uint16_t newHeight);
		friend ostream& operator<<(ostream& os, const TGAImage& tgaImage);

	private:
		enum ColorCoding : uint8_t { NO_TYPE, COLOR_MAPPED, RGB, BLACK_AND_WHITE };

#pragma pack (1)
		struct Header {
			uint8_t idLength;
			uint8_t colorMapType;
			ColorCoding colorCoding : 3;
			bool rleCompression : 5;
			uint16_t colorMapOrigin;
			uint16_t colorMapLength;
			uint8_t colorMapDepth;
			uint16_t xOrigin;
			uint16_t yOrigin;
			uint16_t width;
			uint16_t height;
			uint8_t bitsPerPixel;
			uint8_t alphaBits : 4;
			uint8_t : 4; // screen origin and interleaving flags, which we don't care about
		};
#pragma pack()

		// Raw TGA image data. Could be indexes into a color map or raw color values.
		struct Bitmap : public Array<uint8_t> {

			Bitmap(const size_t width, const size_t height, const size_t bytesPerPixel);

			uint16_t getColormapEntryIdx(const size_t x, const size_t y) const;

			void setColormapEntryIdx(const size_t x, const size_t y, size_t entry);

		};

		// Maps an index to a color
		struct Colormap : public Array<uint8_t> {

			Colormap(const size_t nEntries, const size_t bytesPerEntry);

			void updateRawImageFromEntries();
			uint16_t getClosestEntryIdx(const Color& color) const;

			const RawImage rawImage;

		};

		static Color makeColor(const uint8_t byte0);
		static Color makeColor(const uint8_t byte0, const uint8_t byte1);
		static Color makeColor(const uint8_t byte0, const uint8_t byte1, const uint8_t byte2, const uint8_t byte3 = 0);
		uint8_t getByte(const size_t x, const size_t y, size_t b) const;
		void writeColorBytes(const Color& color, const size_t x, const size_t y);
		void updateRawImageFromBitmap();
		void updateBitmapFromRawImage();

		Header header;
		unique_ptr<Colormap> colormap;
		unique_ptr<Bitmap> bitmap;
		unique_ptr<RawImage> rawImage;

	};

}