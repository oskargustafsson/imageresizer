#include "stdafx.h"
#include "TGAImage.h"

namespace ir {

	TGAImage::Bitmap::Bitmap(const size_t width, const size_t height, const size_t bytesPerPixel) :
		Array(width, height, bytesPerPixel) {}

	uint16_t TGAImage::Bitmap::getColormapEntryIdx(const size_t x, const size_t y) const {
		switch (depth) {
		case 1: return (*this)(x, y, 0);
		case 2: return ((*this)(x, y, 1) << 8) | (*this)(x, y, 0);
		default: throw invalid_argument("Indexes of size > 2 bytes are not supported");
		}
	}

	void TGAImage::Bitmap::setColormapEntryIdx(const size_t x, const size_t y, size_t entry) {
		switch (depth) {
		case 2: (*this)(x, y, 1) = uint8_t(entry >> 8); // Fallthrough
		case 1: (*this)(x, y, 0) = uint8_t(entry & 0xFF); break;
		default: throw invalid_argument("Indexes of size > 2 bytes are not supported");
		}
	}

}