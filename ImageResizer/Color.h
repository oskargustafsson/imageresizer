#pragma once

namespace ir {

	class Color {

	public:
		Color(const size_t a = 0, const size_t r = 0, const size_t g = 0, const size_t b = 0);

		void set(const Color& c);
		size_t getDistanceSq(const Color& c) const;
		Color& operator+=(const Color& rhs);
		Color& operator/=(const size_t rhs);

		size_t a, r, g, b;

	};


}