#include "stdafx.h"
#include "Color.h"

namespace ir {

	Color::Color(const size_t a, const size_t r, const size_t g, const size_t b) :
		a(a), r(r), g(g), b(b) {}

	void Color::set(const Color& c) {
		a = c.a; r = c.r; g = c.g; b = c.b;
	}

	size_t Color::getDistanceSq(const Color& c) const {
		return
			(a - c.a) * (a - c.a) +
			(r - c.r) * (r - c.r) +
			(g - c.g) * (g - c.g) +
			(b - c.b) * (b - c.b);
	}

	Color& Color::operator+=(const Color& rhs) {
		a += rhs.a; r += rhs.r; g += rhs.g; b += rhs.b;
		return *(this);
	}

	Color& Color::operator/=(const size_t rhs) {
		a /= rhs; r /= rhs; g /= rhs; b /= rhs;
		return *(this);
	}

}