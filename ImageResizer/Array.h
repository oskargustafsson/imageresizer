#pragma once

namespace ir {

	template <typename T> class Array {

	public:
		Array(const size_t width = 1, const size_t height = 1, const size_t depth = 1) :
			width(width),
			height(height),
			depth(depth),
			nItems(width * height * depth),
			data(new T[width * height * depth]) {}

		~Array() {
			delete[] data;
		}

		T& operator()(const size_t x = 0, const size_t y = 0, const size_t z = 0) const {
			// (z, x, y) ordering, that's how TGA data is layed out.
			// TODO: better solution
			return data[depth * (width * y + x) + z];
		}

		T* data;
		size_t width, height, depth;
		size_t nItems;

	};




}