#include "stdafx.h"
#include "TGAImage.h"

namespace ir {

	using namespace std;

	TGAImage::TGAImage(ifstream& ifs) {
		ifs.read(reinterpret_cast<char*>(&header), sizeof(header));
		// Discard ID field
		// TODO: Don't discard
		ifs.ignore(header.idLength);
		header.idLength = 0;

		RawImage(header.width, header.height);

		bitmap = make_unique<Bitmap>(header.width, header.height, (header.bitsPerPixel + 7) / 8);
		rawImage = make_unique<RawImage>(header.width, header.height);

		if (header.rleCompression) {
			throw invalid_argument("RLE compressed images are not supported");
		}

		switch (header.colorCoding) {
		case RGB:
		case BLACK_AND_WHITE:
			break;
		case COLOR_MAPPED:
			colormap = make_unique<Colormap>((header.colorMapDepth + 7) / 8, header.colorMapLength);
			ifs.read(reinterpret_cast<char*>(colormap->data), colormap->nItems);
			colormap->updateRawImageFromEntries();
			break;
		default:
			throw invalid_argument("Unsupported data type.");
		}

		ifs.read(reinterpret_cast<char*>(bitmap->data), bitmap->nItems);
		updateRawImageFromBitmap();
	}

	void TGAImage::scale(const float scale) {
		this->scale(uint16_t(header.width * scale), uint16_t(header.height * scale));
	}

	void TGAImage::scale(const uint16_t newWidth, const uint16_t newHeight) {
		header.width = newWidth;
		header.height = newHeight;
		rawImage->scale(newWidth, newHeight);
		updateBitmapFromRawImage();
	}

	ostream& operator<<(ostream& os, const TGAImage& tgaImage) {
		os.write(reinterpret_cast<const char*>(&tgaImage.header), sizeof(tgaImage.header));
		if (tgaImage.header.colorCoding == TGAImage::ColorCoding::COLOR_MAPPED) {
			os.write(reinterpret_cast<const char*>(tgaImage.colormap->data), tgaImage.colormap->nItems);
		}
		os.write(reinterpret_cast<const char*>(tgaImage.bitmap->data), tgaImage.bitmap->nItems);
		return os;
	}

	uint8_t TGAImage::getByte(const size_t x, const size_t y, size_t b) const {
		const auto& bitmap = *(this->bitmap);
		const auto& colormap = *(this->colormap);
		switch (header.colorCoding) {
		case COLOR_MAPPED: return colormap(b, bitmap.getColormapEntryIdx(x, y));
		default: return bitmap(x, y, b);
		}
	}

	Color TGAImage::makeColor(const uint8_t byte0) {
		// Assume B&W image, according to TGA format spec.
		// Arbitrarily store value in alpha channel.
		return Color(byte0, 0, 0, 0);
	}

	Color TGAImage::makeColor(const uint8_t byte0, const uint8_t byte1) {
		// Parse according to http://www.gamers.org/dEngine/quake3/TGA.txt
		// First byte = GGGBBBBB, second byte = ARRRRRGG
		return Color(
			(byte1 >> 7) & 0b1,
			(byte1 >> 2) & 0b11111,
			((byte1 << 3) & 0b11000) | ((byte0 >> 5) & 0b111),
			byte0 & 0b11111);
	}

	Color TGAImage::makeColor(uint8_t byte0, uint8_t byte1, uint8_t byte2, uint8_t byte3) {
		return Color(byte3, byte2, byte1, byte0);
	}

	void TGAImage::updateRawImageFromBitmap() {
		const auto bpp = header.colorCoding == TGAImage::ColorCoding::COLOR_MAPPED ?
			colormap->width : bitmap->depth;
		const auto& rawImage = *(this->rawImage);

//#pragma omp parallel for schedule(dynamic, 1)
		for (auto y = 0; y < rawImage.height; ++y) {
			for (auto x = 0u; x < rawImage.width; ++x) {
				Color& color = rawImage(x, y);
				switch (bpp) {
				case 1:
					color.set(makeColor(
						getByte(x, y, 0)));
					break;
				case 2:
					color.set(makeColor(
						getByte(x, y, 0),
						getByte(x, y, 1)));
					break;
				case 3:
					color.set(makeColor(
						getByte(x, y, 0),
						getByte(x, y, 1),
						getByte(x, y, 2)));
					break;
				case 4:
					color.set(makeColor(
						getByte(x, y, 0),
						getByte(x, y, 1),
						getByte(x, y, 2),
						getByte(x, y, 3)));
					break;
				}
			}
		}
	}

	void TGAImage::updateBitmapFromRawImage() {
		const auto& rawImage = *(this->rawImage);
		if (bitmap->width != rawImage.width || bitmap->height != rawImage.height) {
			bitmap.reset(new Bitmap(uint16_t(rawImage.width), uint16_t(rawImage.height), bitmap->depth));
		}
		auto& bitmap = *(this->bitmap);

//#pragma omp parallel for schedule(dynamic, 1)
		for (auto y = 0; y < rawImage.height; ++y) {
			for (auto x = 0u; x < rawImage.width; ++x) {
				Color& color = rawImage(x, y);
				switch (header.colorCoding) {
				case COLOR_MAPPED:
					bitmap.setColormapEntryIdx(x, y, colormap->getClosestEntryIdx(color));
					break;
				default:
					writeColorBytes(color, x, y);
					break;
				}
			}
		}
	}

	void TGAImage::writeColorBytes(const Color& color, const size_t x, const size_t y) {
		const auto& bitmap = *(this->bitmap);
		switch (bitmap.depth) {
		case 1:
			// Assume B&W image, according to TGA format spec.
			bitmap(x, y, 0) = uint8_t(color.a);
			break;
		case 2:
			// Second byte = ARRRRRGG (alpha bit, red, higher 2 bits of green)
			bitmap(x, y, 1) = uint8_t((color.a << 7) | (color.r << 2) | ((color.g & 0b11000) >> 3));
			// First byte = GGGBBBBB (lower 3 bits of green, blue)
			bitmap(x, y, 0) = uint8_t(((color.g & 0b111) << 5) | color.b);
			break;
		case 3:
			// Assume alphaBits = 0, according to TGA format spec.
			bitmap(x, y, 2) = uint8_t(color.r);
			bitmap(x, y, 1) = uint8_t(color.g);
			bitmap(x, y, 0) = uint8_t(color.b);
			break;
		case 4:
			// Assume alphaBits = 8, according to TGA format spec.
			bitmap(x, y, 3) = uint8_t(color.a);
			bitmap(x, y, 2) = uint8_t(color.r);
			bitmap(x, y, 1) = uint8_t(color.g);
			bitmap(x, y, 0) = uint8_t(color.b);
			break;
		}
	}

}
