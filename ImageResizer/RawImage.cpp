#include "stdafx.h"
#include "RawImage.h"

namespace ir {

	using namespace std;

	void RawImage::scale(const size_t newWidth, const size_t newHeight) {
		const auto invXScale = float(width) / float(newWidth);
		const auto invYScale = float(height) / float(newHeight);

		auto newColors = new Color[newWidth * newHeight];

		// Make sure we get at least one sample from each original image pixel
		const auto nHorizontalSamples = size_t(ceil(invXScale));
		const auto nVerticalSamples = size_t(ceil(invYScale));

		auto collectNewColor = [=](uint16_t x, uint16_t y) {
			Color avgColor;
			for (auto vSample = 0u; vSample < nVerticalSamples; ++vSample) {
				for (auto hSample = 0u; hSample < nHorizontalSamples; ++hSample) {
					const auto xOffset = float(hSample) / float(nHorizontalSamples);
					const auto yOffset = float(vSample) / float(nVerticalSamples);
					avgColor += (*this)(
						uint8_t(invXScale * (x + xOffset)),
						uint8_t(invYScale * (y + yOffset)));
				}
			}
			avgColor /= nHorizontalSamples * nVerticalSamples;
			newColors[newWidth * y + x] = avgColor;
		};

//#pragma omp parallel for schedule(dynamic, 1)
		for (auto y = 0; y < newHeight; ++y) {
			for (auto x = 0u; x < newWidth; ++x) {
				collectNewColor(x, y);
			}
		}

		width = newWidth;
		height = newHeight;
		nItems = width * height;

		delete[] data;
		data = newColors;
	}

}