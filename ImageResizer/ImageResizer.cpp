#include "stdafx.h"
#include "TGAImage.h"

using namespace std;
using namespace ir;

// TODO:
// - RLE compression
// - Color formats
// - Handle errors / exceptions
// - Handle edge cases(extra fields specified in header)
// - Rule of three
// - OMP

int main(int argc, char* argv[]) {
	if (argc < 3) {
		cerr << "Please provide input and output file path arguments." << endl <<
			"E.g. ImageResizer.exe original.tga half.tga\n";
		return 1;
	}

	ifstream ifs(argv[1], ifstream::binary);
	if (!ifs.is_open()) {
		cerr << "Can't open input file.";
		return 1;
	}

	TGAImage* tgaImage;
	try {
		tgaImage = new TGAImage(ifs);
	} catch (const std::exception& e) {
		cerr << e.what();
		return 1;
	}

	tgaImage->scale(0.5f);

	ofstream ofs(argv[2], ofstream::binary);
	if (!ofs.is_open()) {
		cerr << "Can't create/open output file.\n";
		return 1;
	}

	ofs << (*tgaImage);

    return 0;
}
