#include "stdafx.h"
#include "TGAImage.h"

namespace ir {

	TGAImage::Colormap::Colormap(const size_t bytesPerEntry, const size_t nEntries) :
		Array(bytesPerEntry, nEntries),
		rawImage(nEntries) {}

	void TGAImage::Colormap::updateRawImageFromEntries() {
		const auto& me = *this;
		for (auto i = 0u; i < nItems; ++i) {
			switch (width) {
			case 1: rawImage(i).set(makeColor(me(0, i))); break;
			case 2: rawImage(i).set(makeColor(me(0, i), me(1, i))); break;
			case 3: rawImage(i).set(makeColor(me(0, i), me(1, i), me(2, i))); break;
			case 4: rawImage(i).set(makeColor(me(0, i), me(1, i), me(2, i), me(3, i))); break;
			default: throw invalid_argument("Colors wider than 4 bytes are not supported");
			}
		}
	}

	uint16_t TGAImage::Colormap::getClosestEntryIdx(const Color& color) const {
		// TODO: use a spacial data structure to reduce search time from O(n) to O(log(n))
		auto closestIdx = 0u;
		auto closestDistance = SIZE_MAX;
		for (auto entryIdx = 0u; entryIdx < nItems; ++entryIdx) {
			auto& currentColor = rawImage(entryIdx);
			auto distance = color.getDistanceSq(currentColor);
			if (distance < closestDistance) {
				closestDistance = distance;
				closestIdx = entryIdx;
			}
		}
		return closestIdx;
	}

}